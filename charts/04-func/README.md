# First Helm Chart

## Preparation
```bash
git clone https://gitlab.com/ksxack/weather-bot.git
cd weather-bot
cd charts
cp -r 03-conditions/ 04-func
```

## Create Chart
```bash
helm template 04-func/
helm template 04-func/ --debug
```
