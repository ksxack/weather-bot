# First Helm Chart

## Preparation
```bash
git clone https://gitlab.com/ksxack/weather-bot.git
cd weather-bot

mkdir charts
cd charts
```

## Create first Chart
```bash
helm create 01-dummy
```

## Deploy first Helm Release
```bash
kubectl delete ns weather-bot
helm -n weather-dev upgrade --install weather 01-dummy/ --create-namespace
kubectl -n weather-dev get po
```

## Work with Helm Release
```bash
helm -n weather-dev list
helm -n weather-dev ls -a
helm -n weather-dev upgrade --install weather 01-dummy/
kubectl -n weather-dev get po
helm -n weather-dev rollback weather 1
kubectl -n weather-dev get secret
```

